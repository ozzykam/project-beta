# CarCar

Team:

* Aziz K. - Sales Microservice
* Edwinica (Nickie) D. - Services

## Design
CarCar is a web application that helps car dealerships manage their inventory, sales, and service operations. It has three separate microservices: inventory, sales, and service. These microservices use RESTful APIs on the backend to send data to the frontend user interface. Each sales and service microservices has its own instances of the automotive value object (AutomobileVO). These instances are established and updated using specific polling programs that communicate with the inventory microservice, retrieving and updating vehicle data as needed. The entire CarCar application is containerized with Docker, with each microservice operating in its own container for easier deployment and maintenance.

![alt text](image.png)

## Service microservice

When the Automobile Service microservice generates or changes vehicle models or automobiles, it interact with the Inventory microservice's endpoints to retrieve information about manufacturers and vehicle models. This connection ensures that the Automobile Service is up to date on the latest and most accurate information about manufacturers, vehicle models, and autos, allowing for seamless operations such as inventory development, listing, and management.


#### Models
    **Technician**:
    This model represents technicians who handle service appointments. Includes an automated generated identifier for each technician, first_name, last_name, and employee_id.

    **AutomobileVO**:
    This model represents automobiles in the inventory.It includes the vin and a boolean field that determines w*hether the vehicle has been sold or not.

    **Appointment**
    This model represents service appointments for automobiles.This includes its id, date_time, reason for the service appointment, status of the appointment, vin name of the customer and a foreignkey to technician.
#### API
Port 8080:
    Technicians:
List technicians	GET	http://localhost:8080/api/technicians/
Create a technician	POST	http://localhost:8080/api/technicians/
Delete a specific technician	DELETE	http://localhost:8080/api/technicians/:id/

*Example of creating a technician**

JSON body:
{
	"first_name": "John",
	"last_name": "Doe",
	"employee_id": "JD123"
}

Returns:
"technician": [
		{
			"id": 2,
			"first_name": "John",
			"last_name": "Doe",
			"employee_id": "JD123"
		},
]

Appointments:
List appointments	GET	http://localhost:8080/api/appointments/
Create an appointment	POST	http://localhost:8080/api/appointments/
Delete an appointment	DELETE	http://localhost:8080/api/appointments/:id/
Set appointment status to "canceled"	PUT	http://localhost:8080/api/appointments/:id/cancel/
Set appointment status to "finished"	PUT	http://localhost:8080/api/appointments/:id/finish/

*Example of creating an appointment**
{
	"id": 6,
	"date_time": "2024-05-07T10:00:00Z",
	"reason": "Bad engine",
	"status": "New",
	"vin": "12345678912345678",
	"customer": "Nickie",
	"technician": {
		"id": 2,
		"first_name": "John",
		"last_name": "Doe",
		"employee_id": "JD123"
	}
}

Returns:
{
	"appointment": [
		{
			"id": 3,
			"date_time": "2024-05-07T10:00:00+00:00",
			"reason": "Bad engine",
			"status": "New",
			"vin": "12345678912345678",
			"customer": "Nickie",
			"technician": {
				"id": 2,
				"first_name": "John",
				"last_name": "Doe",
				"employee_id": "JD123"
			}
		},
    ]
}

## Sales Microservice
This microservice manages and tracks the sales of automobiles. It encapsulates details about the sales, customers, salespeople, and the automobiles themselves, providing a comprehensive system for monitoring automobile transactions.

>>># NOTICE!<br>
>>> User must submit at least one (1) "Manufacturer", one (1) "Model" (aka, vehicle model type), one (1) "Automobile", one (1) "Customer", and one (1) "Salesperson" into the database before the sale service can operate. Any attempts to create a NEW SALE will return an error without the ability to reference an existing automobile, salesperson, and customer.

>## Main Components

#### **Models in `root/sales/api/sales-rest/models.py`**
- **Salesperson**: Details of the salesperson, including their name, contact info, and employee ID. Names are automatically formatted as "Lastname, Firstname" upon saving. Employee IDs are alos automatically formatted as "Initial.LASTNAME-**last 4 numbers of phone number**
- **Customer**: Stores customer information including a specially formatted name, contact details, and address. Names are formatted as "Lastname, Initials."
- **Sale**: Links together an automobile, a salesperson, and a customer for each transaction, storing the sale price and other relevant details.
- **AutomobileVO**: Represents vehicles in inventory, with attributes like VIN (Vehicle Identification Number) and sold status.
*A NOTE ABOUT AUTOMOBILEVO*: AutomobileVO is a value object which stores the VIN, Sold Status, and Href of the corresponding <strong>Automobile</strong> model in the <strong>Inventory Microservice</strong> via polling every 60 seconds (as defined in `root/sales/poller/poller.py`)

#### Encoders (JSON Serializers)
- **AutomobileVOEncoder, SalespersonCreateEncoder, CustomerCreateEncoder, etc.**: Specialized serializers for converting model instances into JSON format suitable for API responses. For example, `SaleEncoder` includes the vehicle's VIN, the salesperson's details, and the customer's name in the sales data.
<br>
<br>
<br>
<br>

> ## API Endpoints and Outputs: *Port 8090*
*The Sales model is dependent on data which is stored in the database after being created by the Automobile model in the Inventory Microservice as well as the data  stored in the database after being created by the "Salespoeple" and "Customer" models in the Sales Microservice.*<br>
<strong>Note: order of creation must go as follows:</strong><br>
- <strong>1st Automobiles:</strong> *which is located in the Inventory microservice, and is dependent on the "Manufacturer" and "Models" models -- both of which are also located in the Inventory microservice*
- <strong>2nd Customers:</strong> *which has no dependencies*
- <strong>3rd Salespeople:</strong> *which has no dependencies*
- <strong>4th Sales:</strong> *which is dependent on the Automobiles, Customers, and Salespeople models, as each creation of a new Sale instance requires an existing/valid <strong>Automobile VIN</strong>, <strong>Employee ID</strong>, and <strong>Customer Name</strong>*

###  "Salespeople"
**Location:** `http://localhost:8090/api/salespeople/`<br>
Returns a list of all salespeople, including their names formatted as "Lastname, Firstname" and their employee IDs.

> *Example input:*

```json
{
	"first_name": "Betty",
	"last_name": "White",
	"phone_number": 6515295765
}
```
>*Example output:*
```json
{
	"salespeople": [
		{
			"id": 1,
			"first_name": "Betty",
			"last_name": "White",
			"employee_id": "B.WHITE-5765",
			"name": "White, Betty"
		}
    ]
}
```
<br>

### "Customers"
**Location:**`http://localhost:8090/api/customers/`<br>
Lists all customers, showing their specially formatted names, phone numbers, and addresses.
>*Example input:*
```json
{
	"first_name": "Bruce",
	"last_name": "Wayne",
	"phone_number": 6128675309,
	"address": "118 Batcave Drive, Springfield, IL"
}
```
>*Example output:*
```json
{
	"customer": [
		{
			"id": 1,
			"name": "WAYNE, B.",
			"phone_number": "6128675309",
			"address": "118 Batcave Drive, Springfield, IL"
		}
	]
}
```
<br>

### "Sales"
 **Location:** `http://localhost:8090/api/sales/`<br>
Details individual sales transactions, including the salesperson involved, the customer, and the automobile details along with whether it's been sold.
>**Example input:**
```json
{
	"price": 5050000,
	"automobile": "1C3CC5FB2AN120176",
	"salesperson": "D.JOHNSON-5766",
	"customer": "UZAMAKI, N."
}
```
>**Example output:**
```json
{
	"sales": [
		{
			"id": 10,
			"price": 51024.0,
			"automobile": "1C3CC5FB2AN120176",
			"sold": true,
			"salesperson_id": "N.UZAMAKI-5767",
			"salesperson_name": "Uzamaki, Naruto",
			"customer": "JOHNSON, D."
		}
	]
}
```
<br>
<br>

> ## API Endpoints and Outputs: *Port 8100*
*The information and details pertaining to the automobiles which are referenced in the Sales Microservice are created here in the "Manufacturers", "Models", and "Automobiles" models in the Inventory Microservice.<br>
<strong>Note: order of creation must go as follows:</strong><br>
- <strong>1st: Manufacturers</strong> (which has no dependeancies)
- <strong>2nd: Models</strong> (which relies on an existing <strong>manufacturer</strong> id)
- <strong>3rd: Automobiles</strong> (which relies on an existing <strong>models</strong> id)*

###  "Manufacturers"

**Location:** `http://localhost:8100/api/manufacturers/`<br>
[insert stuff here]
> *Example input:*

```json
{
  "name": "Chrysler"
}
```
>*Example output:*
```json
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	]
}
```
<br>

### "Models"
**Location:** `http://localhost:8100/api/models/`<br>
[insert stuff here]
> *Example input:*

```json
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```
>*Example output:*
```json
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Sebring",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Chrysler"
			}
		}
	]
}
```
<br>

### "Automobiles"
**Location:** `http://localhost:8100/api/automobiles/`<br>
[insert stuff here]
> *Example input:*

```json
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```
>*Example output:*
```json
{
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2AN120174/",
			"id": 1,
			"color": "red",
			"year": 2012,
			"vin": "1C3CC5FB2AN120174",
			"model": {
				"href": "/api/models/2/",
				"id": 2,
				"name": "Ram",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/2/",
					"id": 2,
					"name": "Dodge"
				}
			},
			"sold": false
		}
	]
}
```
