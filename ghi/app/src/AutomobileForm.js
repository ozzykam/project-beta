import React, { useState, useEffect } from 'react'

function AutomobileForm() {
    const [models, setModels] = useState([])
    const [formData, setFormData] = useState({
        model_id: '',
        vin: '',
        color: '',
        year: '',
    })

    const getData = async () => {
        const model_url = 'http://localhost:8100/api/models/'
        const model_response = await fetch(model_url)

        if(model_response.ok) {
            const data = await model_response.json()
            setModels(data.models)
        }
        
    }

    useEffect(() => {
        getData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()

        const automobileUrl = 'http://localhost:8100/api/automobiles/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        }
        const response = await fetch(automobileUrl, fetchConfig)
        if (response.ok) {
            console.log('Automobile created successfully', await response.json())
            setFormData({
                model: '',
                vin: '',
                color: '',
                year: '',
            })
        } else {
            throw new Error('failed to create AUTOMOBILE')
        }
    }
    

    const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name
        
        setFormData({
            ...formData,
            [inputName]: value
        })
    }
    
    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add an Automobile to the Inventory </h1>
                        <form onSubmit={handleSubmit} id="create-sale-form">
                            <div className="mb-3 mt-4">
                                <label htmlFor="model_id">Select Vehicle Model...</label>
                                <select onChange={handleFormChange} value={formData.model_id} required name="model_id" id="model_id" className="form-select">
                                    <option value="">Select</option>
                                    {models.map(model => {
                                        return (
                                            <option key={model.model_id} value={model.id}>{model.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3 mt-4">
                                <input 
                                type="text" 
                                className="form-control" 
                                id="color"
                                name="color"
                                placeholder="Color"
                                value={formData.color}
                                onChange={handleFormChange}
                                required
                                />
                                <label htmlFor="color">Color...</label>
                            </div>
                            <div className="form-floating mb-3 mt-4">
                                <input 
                                type="number" 
                                className="form-control" 
                                id="year"
                                name="year"
                                placeholder="Year"
                                value={formData.year}
                                onChange={handleFormChange}
                                required
                                />
                                <label htmlFor="color">Year</label>
                            </div>
                            <div className="form-floating mb-3 mt-4">
                                <input 
                                type="text" 
                                className="form-control" 
                                id="vin"
                                name="vin"
                                placeholder="VIN"
                                value={formData.vin}
                                onChange={handleFormChange}
                                required
                                />
                                <label htmlFor="color">Vehicle VIN...</label>
                            </div>
                            <button className="btn btn-primary">Add Automobile</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )

}

export default AutomobileForm