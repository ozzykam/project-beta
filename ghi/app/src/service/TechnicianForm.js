import { useState } from 'react';

function TechnicianForm() {
    const [first_name, setFirstName] = useState("");
    const [last_name, setLastName] = useState("");
    const [employee_id, setEmployeeID] = useState("");

    async function handleSubmit(event, first_name, last_name, employee_id){
        event.preventDefault();
        const data = {}
        data.first_name = first_name
        data.last_name = last_name
        data.employee_id = employee_id

        const technicianUrl = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            setFirstName("");
            setLastName("");
            setEmployeeID("");
        } else {
            alert("An error occured while adding technician.");
        }

    }

    function handleFirstName(e) {
        const value = e.target.value;
        setFirstName(value);
    }

    function handleLastName(e) {
        const value = e.target.value;
        setLastName(value);
    }

    function handleEmployeeID(e) {
        const value = e.target.value;
        setEmployeeID(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Technician</h1>
                    <form  onSubmit={(event) => handleSubmit(event, first_name, last_name, employee_id)} id="Add-Technician-Form">
                        <div className="form-floating mb-3">
                            <input onChange ={handleFirstName}  value = {first_name} placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="First Name">First name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange ={handleLastName}  value = {last_name} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="Last Name">Last name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange ={handleEmployeeID}  value = {employee_id} placeholder="employee_id" required type="text" name="employee_id" id="employee_id" className="form-control" />
                            <label htmlFor="name">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
    }

    export default TechnicianForm;
