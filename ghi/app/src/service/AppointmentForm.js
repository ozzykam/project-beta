import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function AppointmentForm() {
    const [vin, setVin] = useState("");
    const [customer, setCustomer] = useState("");
    const [date_time, setDateTime] = useState("");
    const [firstname, setFirstName] = useState("");
    const [lastname, setLastName] = useState("");
    const [employeeId, setEmployeeId] = useState("");
    const [reason, setReason] = useState("");


    const navigate = useNavigate()
    const handleSubmit = async (event) => {
        event.preventDefault();
        console.log("im working!!")

        const appointmentData = {
            vin,
            customer,
            date_time,
            technician: {
            first_name: firstname,
  	        last_name:lastname,
            employee_id:employeeId
            },
            status: "New",
            reason
        };
        console.log('appointment Data',appointmentData)

        await fetch('http://localhost:8080/api/appointments/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(appointmentData)
        }).then((res)=>{
            if (res.status === 200) {
                navigate('/appointments')
            } else {
                return
            }
        });

        setVin("");
        setCustomer("");
        setDateTime("");
        setFirstName("");
        setLastName("");
        setEmployeeId("");
        setReason("");

    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1 className="text-center">Create Service Appointment</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                value={vin}
                                onChange={(e) => setVin(e.target.value)}
                                placeholder="VIN"
                                required
                                type="text"
                                name="vin"
                                id="vin"
                                className="form-control"
                            />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={customer}
                                onChange={(e) => setCustomer(e.target.value)}
                                placeholder="Customer Name"
                                required
                                type="text"
                                name="customerName"
                                id="customerName"
                                className="form-control"
                            />
                            <label htmlFor="customerName">Customer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={date_time}
                                onChange={(e) => setDateTime(e.target.value)}
                                placeholder="Date and Time"
                                required
                                type="datetime-local"
                                name="dateTime"
                                id="dateTime"
                                className="form-control"
                            />
                            <label htmlFor="dateTime">Date and Time</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={firstname}
                                onChange={(e) => setFirstName(e.target.value)}
                                placeholder="Firstanme"
                                required
                                type="text"
                                name="firstname"
                                id="firstname"
                                className="form-control"
                            />
                            <label htmlFor="firstname">Technician's First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={lastname}
                                onChange={(e) => setLastName(e.target.value)}
                                placeholder="Technician"
                                required
                                type="text"
                                name="lastname"
                                id="lastname"
                                className="form-control"
                            />
                            <label htmlFor="lastname">Technician's Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={employeeId}
                                onChange={(e) => setEmployeeId(e.target.value)}
                                placeholder="Technician"
                                required
                                type="text"
                                name="employeeId"
                                id="employeeId"
                                className="form-control"
                            />
                            <label htmlFor="employeeId">Employee Id</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea
                                value={reason}
                                onChange={(e) => setReason(e.target.value)}
                                placeholder="Reason"
                                required
                                name="reason"
                                id="reason"
                                className="form-control"
                                style={{ height: '100px' }}
                            />
                            <label htmlFor="reason">Reason for Appointment</label>
                        </div>
                        <div className="col text-center">
                            <button type="submit" className="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AppointmentForm;
