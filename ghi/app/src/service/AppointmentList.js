import React, { useState, useEffect } from 'react';

function AppointmentList() {
    const [appointments, setAppointments] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);
    const [soldVins, setSoldVins] = useState([]);

    async function loadSoldVins() {
        const response = await fetch ("http://localhost:8080/api/appointments/sold/")

        if (response.ok) {
            const data = await response.json()

            const soldVinsOnly = data.map(auto => auto.vin)

            setSoldVins(soldVinsOnly)
        }
    }

    async function loadAppointments() {
        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const appointmentResponse = await fetch(appointmentUrl);

        if (appointmentResponse.ok) {
            const appointmentData = await appointmentResponse.json();

            const hasVin = (vin) => appointmentData.appointment.indexOf(appointment => appointment.vin === vin);
            console.log(hasVin("12345"))

            setAppointments(appointmentData.appointment);
            setAutomobiles(appointmentData.appointment.map(appointment => ({ vin: appointment.vin, isVip: hasVin(appointment.vin) })));
        }
        for (let i = 0; i < soldVins.length; i++) {
            const a = automobiles.filter(auto => auto.vin === soldVins[i])
            console.log(a)
        }
    }

    async function cancelAppointment(appointmnetID) {
        console.log(appointmnetID)
        const apptURL = `http://localhost:8080/api/appointments/${appointmnetID}/cancel/`;
        const fetchOptions = {
            method: "PUT",
            body: JSON.stringify({ status: "Cancelled" }),
            headers: { 'Content-Type': 'application/json' }
        };
        const response = await fetch(apptURL, fetchOptions);

        if (response.ok) {
            alert('Appointment successfully cancelled!');
            loadAppointments();
        } else {
            alert('An error occurred while cancelling the appointment.');
        }
    }

    async function finishAppointment(appointmentID) {
        const apptURL = `http://localhost:8080/api/appointments/${appointmentID}/finish/`;
        const fetchOptions = {
            method: "PUT",
            body: JSON.stringify({ status: "Finished" }),
            headers: { 'Content-Type': 'application/json' }
        };
        const response = await fetch(apptURL, fetchOptions);

        if (response.ok) {
            alert('Appointment successfully finished!');
            loadAppointments();
        } else {
            alert('An error occurred while finishing the appointment.');
        }
    }

    useEffect(() => {
        loadSoldVins();
        loadAppointments();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {appointments.length > 0 && appointments.map(appointment => {
                    const automobile = automobiles.filter(auto => auto);
                    const soldCars = soldVins.includes(appointment.vin)

                    return (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{soldCars ? "Yes" : "No"}</td>
                            <td>{appointment.customer}</td>
                            <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                            <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                            <td>{appointment.technician.first_name + " " + appointment.technician.last_name}</td>
                            <td>{appointment.reason}</td>
                            <td>
                                    <>
                                        <button className="btn btn-outline-danger" onClick={() => cancelAppointment(appointment.id)}>Cancel</button>
                                        <button className="btn btn-outline-success" onClick={() => finishAppointment(appointment.id)}>Finish</button>
                                    </>


                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}
export default AppointmentList;
