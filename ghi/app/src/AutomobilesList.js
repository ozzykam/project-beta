import React, { useState, useEffect } from 'react'

function AutomobilesList(props) {
    const [autos, setAutos] = useState(props.automobiles || [])

    useEffect (() => {
        setAutos(props.automobiles)
    }, [props.automobiles])

    return (
        <>
        <h1 className="display-6 fw-bold">Automobiles</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                </tr>
            </thead>
            <tbody>
                {autos && autos.map(auto => (
                    <tr key={auto.id}>
                        <td>{auto.vin}</td>
                        <td>{auto.color}</td>
                        <td>{auto.year}</td>
                        <td>{auto.model.name}</td>
                        <td>{auto.model.manufacturer.name}</td>
                        <td>{auto.sold ? "Yes" : "No"}</td>
                    </tr>
                ))}
            </tbody>
        </table>
        </>
    )
}

export default AutomobilesList