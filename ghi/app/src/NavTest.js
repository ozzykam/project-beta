import { NavLink } from 'react-router-dom';
import { useState } from 'react';
import './Nav.css'

function Nav() {
  const [openAccordion, setOpenAccordion] = useState(null);

  const toggleAccordion = (id) => {
    if (openAccordion === id) {
      setOpenAccordion(null)
    } else {
      setOpenAccordion(id);
    }
  };

  
  const closeAccordion = () => {
    setOpenAccordion(null); 
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <div className="accordion" id="accordionExample">
              <div className="accordion-item bg-success">
                <h2 className="accordion-header" id="headingSalespeople">
                  <button className="accordion-button collapsed bg-success text-white" type="button"
                    onClick={() => toggleAccordion('Salespeople')}
                    aria-expanded={openAccordion === 'Salespeople'}
                    aria-controls="collapseSalespeople">
                    Salespeople
                  </button>
                </h2>
                <div id="collapseSalespeople" className={`accordion-collapse collapse ${openAccordion === 'Salespeople' ? 'show' : ''}`}
                     aria-labelledby="headingSalespeople">
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item bg-success">
                      <NavLink onClick={closeAccordion} className="nav-link text-white" to="/salespersons/new">  Add a Salesperson</NavLink>
                    </li>
                    <li className="list-group-item bg-success">
                      <NavLink onClick={closeAccordion} className="nav-link text-white" to="/salespersons">  Salespeople List</NavLink>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="accordion-item bg-success">
                <h2 className="accordion-header" id="headingCustomers">
                  <button className="accordion-button collapsed bg-success text-white" type="button"
                    onClick={() => toggleAccordion('Customers')}
                    aria-expanded={openAccordion === 'Customers'}
                    aria-controls="collapseCustomers">
                    Customers
                  </button>
                </h2>
                <div id="collapseCustomers" className={`accordion-collapse collapse ${openAccordion === 'Customers' ? 'show' : ''}`}
                     aria-labelledby="headingCustomers">
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item bg-success">
                      <NavLink onClick={closeAccordion} className="nav-link text-white" to="/customers/new">Add a Customer</NavLink>
                    </li>
                    <li className="list-group-item bg-success">
                      <NavLink onClick={closeAccordion} className="nav-link text-white" to="/customers">Customers List</NavLink>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="accordion-item bg-success">
                <h2 className="accordion-header" id="headingSales">
                  <button className="accordion-button collapsed bg-success text-white" type="button"
                    onClick={() => toggleAccordion('Sales')}
                    aria-expanded={openAccordion === 'Sales'}
                    aria-controls="collapseSales">
                    Sales
                  </button>
                </h2>
                <div id="collapseSales" className={`accordion-collapse collapse ${openAccordion === 'Sales' ? 'show' : ''}`}
                     aria-labelledby="headingSales">
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item bg-success">
                      <NavLink onClick={closeAccordion} className="nav-link text-white" to="/sales/new">Record a Sale</NavLink>
                    </li>
                    <li className="list-group-item bg-success">
                      <NavLink onClick={closeAccordion} className="nav-link text-white" to="/sales">Sales List</NavLink>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="accordion-item bg-success">
                <h2 className="accordion-header" id="headingManufacturers">
                  <button className="accordion-button collapsed bg-success text-white" type="button"
                    onClick={() => toggleAccordion('Manufacturers')}
                    aria-expanded={openAccordion === 'Manufacturers'}
                    aria-controls="collapseManufacturers">
                    Manufacturers
                  </button>
                </h2>
                <div id="collapseManufacturers" className={`accordion-collapse collapse ${openAccordion === 'Manufacturers' ? 'show' : ''}`}
                     aria-labelledby="headingManufacturers">
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item bg-success">
                      <NavLink onClick={closeAccordion} className="nav-link text-white" to="/manufacturers/new">Add a Manufacturer</NavLink>
                    </li>
                    <li className="list-group-item bg-success">
                      <NavLink onClick={closeAccordion} className="nav-link text-white" to="/manufacturers">Manufacturers List</NavLink>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="accordion-item bg-success">
                <h2 className="accordion-header" id="headingVehicleModels">
                  <button className="accordion-button collapsed bg-success text-white" type="button"
                    onClick={() => toggleAccordion('Vehicle Models')}
                    aria-expanded={openAccordion === 'Vehicle Models'}
                    aria-controls="collapseVehicleModels">
                    Vehicle Models
                  </button>
                </h2>
                <div id="collapseVehicleModels" className={`accordion-collapse collapse ${openAccordion === 'Vehicle Models' ? 'show' : ''}`}
                     aria-labelledby="headingVehicleModels">
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item bg-success">
                      <NavLink onClick={closeAccordion} className="nav-link text-white" to="/vehicle-models/new">Add a Vehicle Model</NavLink>
                    </li>
                    <li className="list-group-item bg-success">
                      <NavLink onClick={closeAccordion} className="nav-link text-white" to="/vehicle-models">Vehicle Model List</NavLink>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="accordion-item bg-success">
                <h2 className="accordion-header" id="headingAutomobiles">
                  <button className="accordion-button collapsed bg-success text-white" type="button"
                    onClick={() => toggleAccordion('Automobiles')}
                    aria-expanded={openAccordion === 'Automobiles'}
                    aria-controls="collapseAutomobiles">
                    Automobiles
                  </button>
                </h2>
                <div id="collapseAutomobiles" className={`accordion-collapse collapse ${openAccordion === 'Automobiles' ? 'show' : ''}`}
                     aria-labelledby="headingAutomobiles">
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item bg-success">
                      <NavLink onClick={closeAccordion} className="nav-link text-white" to="/automobiles/new">Add an Automobile</NavLink>
                    </li>
                    <li className="list-group-item bg-success">
                      <NavLink onClick={closeAccordion} className="nav-link text-white" to="/automobiles">Automobiles List</NavLink>
                    </li>
                  </ul>
                </div>
              </div>

              {/* Add more sections as necessary */}
              
            </div>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
