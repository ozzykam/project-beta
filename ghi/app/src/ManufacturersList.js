import React, { useState, useEffect } from 'react'

function Manufacturers(props) {
    const [manufacturers, setManufacturers] = useState(props.manufacturers || [])

    useEffect(() => {
        setManufacturers(props.manufacturers)
    }, [props.manufacturers])

    return(
        <>
        <h1 className="display-6 fw-bold">Manufacturers</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer Name</th>
                </tr>
            </thead>
            <tbody>
                {manufacturers && manufacturers.map(manufacturer => (
                    <tr key={manufacturer.href}>
                        <td>{manufacturer.name}</td>
                    </tr>
                ))}
            </tbody>
        </table>
        </>
    )

}

export default Manufacturers