import React, { useState, useEffect } from 'react'

function Customers(props) {
    const [customers, setCustomers] = useState(props.customers || [])

    useEffect (() => {
        setCustomers(props.customers)
    }, [props.customers])

    return (
        <>
        <h1 className="display-6 fw-bold">Customers</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Address</th>
                </tr>
            </thead>
            <tbody>
                {customers && customers.map(customer => (
                    <tr key={customer.id}>
                        <td>{customer.name}</td>
                        <td>{customer.phone_number}</td>
                        <td>{customer.address}</td>
                    </tr>
                ))}
            </tbody>
        </table>
        </>
    )
}

export default Customers