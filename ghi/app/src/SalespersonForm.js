import React, { useState, useEffect } from 'react'

function SalespersonForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        phone_number: '',
        employee_id: '',
    })

    useEffect(() => {
        if(formData.first_name && formData.last_name) {
            setFormData(prevState => ({
                ...prevState,
                name: `${formData.last_name}, ${formData.first_name}`
            }))
        }
        if(formData.first_name && formData.last_name && formData.phone_number) {
            setFormData(prevState => ({
                ...prevState,
                employee_id: `${formData.first_name[0].toUpperCase()}.${formData.last_name.toUpperCase()}-${formData.phone_number.slice(-4)}`
            }))
        }
    }, [formData.first_name, formData.last_name, formData.phone_number])

    const handleInputChange = (event) => {
        const { name, value } = event.target
        setFormData(prevState => ({
            ...prevState,
            [name]: value
        }))
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        
        const response = await fetch(salespeopleUrl, fetchConfig)
        if (response.ok) {     
            setFormData({
                first_name: '',
                last_name: '',
                phone_number: '',
                employee_id: '',
            })
        } else {
            throw new Error('Failed to create SALESPERSON')
        }
        
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Salesperson</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floating mb-3">
                            <input 
                            type="text" 
                            className="form-control" 
                            id="first_name"
                            name="first_name"
                            placeholder="First Name"
                            value={formData.first_name}
                            onChange={handleInputChange}
                            required
                            />
                            <label htmlFor="first_name">First name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input 
                            type="text" 
                            className="form-control" 
                            id="last_name"
                            name="last_name"
                            placeholder="Last Name"
                            value={formData.last_name}
                            onChange={handleInputChange}
                            required
                            />
                            <label htmlFor="last_name">Last name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input 
                            type="tel" 
                            className="form-control" 
                            id="phone_number"
                            name="phone_number"
                            placeholder="Phone Number"
                            value={formData.phone_number}
                            onChange={handleInputChange}
                            required
                            />
                            <label htmlFor="phone_number">Phone number...</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="name">Employee ID</label>
                            <input 
                            type="text" 
                            className="form-control" 
                            id="employee_id"
                            name="employee_id"
                            placeholder="Employee ID"
                            value={formData.employee_id}
                            onChange={handleInputChange}
                            readOnly
                            />
                        </div>
                        <button type="submit" className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    )

}

export default SalespersonForm
