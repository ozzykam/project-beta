import React, { useState, useEffect } from 'react';

function SalesBySalesPerson(props) {
    const [selectedSalesperson, setSelectedSalesperson] = useState('');
    const [filteredSales, setFilteredSales] = useState([]);

    useEffect(() => {
        if (selectedSalesperson) {
            const filtered = props.sales.filter(
                sale => sale.salesperson_name === selectedSalesperson
            );
            setFilteredSales(filtered);
        }
    }, [selectedSalesperson, props.sales]);

    const handleSalespersonChange = (event) => {
        setSelectedSalesperson(event.target.value);
    };

    return (
        <>
            <h2>Salesperson History</h2>
            <div className="mb-3 mt-4" style={{width: '350px'}}>
            <select value={selectedSalesperson} onChange={handleSalespersonChange} className="form-select">
                <option value="">Select a salesperson</option>
                {props.sales && [...new Set(props.sales.map(sale => sale.salesperson_name))].map(name => (
                    <option key={name} value={name}>{name}</option>
                ))}
            </select>
            </div>
            {selectedSalesperson && (
                <>
                    
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Salesperson</th>
                                <th>Customer</th>
                                <th>VIN</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredSales.map(sale => (
                                <tr key={sale.id}>
                                    <td>{sale.salesperson_name}</td>
                                    <td>{sale.customer}</td>
                                    <td>{sale.automobile}</td>
                                    <td>${sale.price.toLocaleString()}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </>
            )}
        </>
    );
}

export default SalesBySalesPerson;
