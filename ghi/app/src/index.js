import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import {BrowserRouter} from 'react-router-dom'

const root = ReactDOM.createRoot(document.getElementById('root'))

async function loadData() {
  const salespeopleResponse = await fetch('http://localhost:8090/api/salespeople/')
  const customersResponse = await fetch('http://localhost:8090/api/customers/')
  const salesResponse = await fetch('http://localhost:8090/api/sales/')
  const manufacturerResponse = await fetch('http://localhost:8100/api/manufacturers/')
  const modelsResponse = await fetch('http://localhost:8100/api/models/')
  const automobilesResponse = await fetch('http://localhost:8100/api/automobiles/')
  if (!salespeopleResponse.ok || !customersResponse.ok || !salesResponse.ok || !manufacturerResponse.ok || !modelsResponse.ok || !automobilesResponse.ok) {
    console.error(`Failed to Fetch ${salespeopleResponse} and/or ${customersResponse} and/or ${salesResponse} and/or ${manufacturerResponse} and/or ${modelsResponse} and/or ${automobilesResponse}`)
  } else {
    const salespeopleData = await salespeopleResponse.json()
    const customersData = await customersResponse.json()
    const salesData = await salesResponse.json()
    const manufacturersData = await manufacturerResponse.json()
    const modelsData = await modelsResponse.json()
    const automobilesData = await automobilesResponse.json()
    root.render(
      <React.StrictMode>
        <BrowserRouter>
          <App
            salespeople={salespeopleData.salespeople}
            customers={customersData.customer}
            sales={salesData.sales}
            manufacturers={manufacturersData.manufacturers}
            models={modelsData.models}
            automobiles={automobilesData.autos}
          />
        </BrowserRouter>
      </React.StrictMode>
    )
    }
  }
  loadData()
