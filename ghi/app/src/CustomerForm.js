import React, { useState, useEffect } from 'react'

function CustomerForm () {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
    })

    useEffect(() => {
        if(formData.first_name && formData.last_name) {
            setFormData(prevState => ({
                ...prevState,
                name: `${formData.last_name.toUpperCase()}, ${String(formData.first_name)[0].toUpperCase()}.`
            }))
        }
    }, [formData.first_name, formData.last_name])

    const handleInputChange = (event) => {
        const { name, value } = event.target
        setFormData(prevState => ({
            ...prevState,
            [name]: value
        }))
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const customerUrl = 'http://localhost:8090/api/customers/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(customerUrl, fetchConfig)
        if (response.ok) {
            console.log('Customer created successfully', await response.json())
            setFormData({
                first_name: '',
                last_name: '',
                address: '',
                phone_number: '',
            })
        } else {
            throw new Error('Failed to create CUSTOMER')
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Customer</h1>
                    <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="form-floating mb-3">
                            <lable htmlFor="last_name">Last Name</lable>
                            <input
                            type="text"
                            className="form-control"
                            id="first_name"
                            name="first_name"
                            placeholder="First Name"
                            value={formData.first_name}
                            onChange={handleInputChange}
                            required
                            />
                        </div>
                        <div className="form-floating mb-3">
                            <lable htmlFor="last_name">Last Name</lable>
                            <input
                            type="text"
                            className="form-control"
                            id="last_name"
                            name="last_name"
                            placeholder="Last Name"
                            value={formData.last_name}
                            onChange={handleInputChange}
                            required
                            />
                        </div>
                        <div className="form-floating mb-3">
                            <lable htmlFor="address">Address</lable>
                            <input
                            type="text"
                            className="form-control"
                            id="address"
                            name="address"
                            placeholder="Address"
                            value={formData.address}
                            onChange={handleInputChange}
                            required
                            />
                        </div>
                        <div className="form-floating mb-3">
                            <lable htmlFor="phone_number">Phone Number</lable>
                            <input 
                            type="tel" 
                            className="form-control" 
                            id="phone_number"
                            name="phone_number"
                            placeholder="Phone Number"
                            value={formData.phone_number}
                            onChange={handleInputChange}
                            required
                            />
                        </div>
                        <button type="submit" className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CustomerForm