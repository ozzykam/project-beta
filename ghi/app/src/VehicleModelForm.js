import React, { useState, useEffect } from 'react'

function VehicleModelForm() {
    const [manufacturers, setManufacturers] = useState([])
    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer_id: '',
    })

    const getData = async () => {
        const manufacturer_url = 'http://localhost:8100/api/manufacturers/'
        const manufacturer_response = await fetch(manufacturer_url)

        if(manufacturer_response.ok) {
            const data = await manufacturer_response.json()
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()

        const vehiclModelsUrl = 'http://localhost:8100/api/models/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        }
        const response = await fetch(vehiclModelsUrl, fetchConfig)
        if (response.ok) {
            console.log('Sale Record created successfully', await response.json())
            setFormData({
                name: '',
                picture_url: '',
                manufacturer_id: '',
            })
        } else {
            throw new Error('failed to create VEHICLE MODEL')
        }
    }
    

    const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name
        
        setFormData({
            ...formData,
            [inputName]: value
        })
    }
    
    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Vehicle Model</h1>
                        <form onSubmit={handleSubmit} id="create-sale-form">
                            <div className="form-floating mb-3 mt-4">
                                <input 
                                type="text" 
                                className="form-control" 
                                id="name"
                                name="name"
                                placeholder="Model Name"
                                value={formData.name}
                                onChange={handleFormChange}
                                required
                                />
                            <label htmlFor="vehicle-model-name">Vehicle Model Name...</label>
                            </div>
                            <div className="form-floating mb-3 mt-4">
                                <input 
                                type="text" 
                                className="form-control" 
                                id="picture_url"
                                name="picture_url"
                                placeholder="Picture Url"
                                value={formData.picture_url}
                                onChange={handleFormChange}
                                required
                                />
                                <label htmlFor="vehicle-model-picture">Picture Url...</label>
                            </div>
                            <div className="mb-3 mt-4">
                                <label htmlFor="manufacturer_id">Choose a Manufacturer</label>
                                <select onChange={handleFormChange} value={formData.manufacturer_id} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                                    <option value="">Select</option>
                                    {manufacturers.map(manufacturer => {
                                        return (
                                            <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary"> Add Sale</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )

}

export default VehicleModelForm