import React, { useState, useEffect } from 'react'

function SalesForm() {
    const [automobiles, setAutomobiles] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [customers, setCustomers] = useState([])
    const [formData, setFormData] = useState({
        automobile: '',
        salesperson: '',
        customer: '',
        price: '',
    })

    const getData = async () => {
        const automobile_url = 'http://localhost:8100/api/automobiles/'
        const automobile_response = await fetch(automobile_url)

        if(automobile_response.ok) {
            const data = await automobile_response.json();
            const unsoldAutos = data.autos.filter(auto => auto.sold === false);
            setAutomobiles(unsoldAutos)
        }
        

        const salespeople_url = 'http://localhost:8090/api/salespeople/'
        const salespeople_response = await fetch(salespeople_url)

        if(salespeople_response.ok) {
            const data = await salespeople_response.json()
            setSalespeople(data.salespeople)
        }

        const customer_url = 'http://localhost:8090/api/customers/'
        const customer_response = await fetch(customer_url)

        if(customer_response.ok) {
            const data = await customer_response.json()
            setCustomers(data.customer)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()

        const salesUrl = 'http://localhost:8090/api/sales/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        }
        const response = await fetch(salesUrl, fetchConfig)
        if (response.ok) {
            console.log('Sale Record created successfully', await response.json())
            const updatedAutomobile = {
                ...formData,
                sold: true,  
            };
    
            const autoUpdateUrl = `http://localhost:8100/api/automobiles/${formData.automobile}/`
            const autoFetchConfig = {
                method: "put", 
                body: JSON.stringify(updatedAutomobile),
                headers: {
                    'Content-Type': 'application/json'
                },
            };
    
            const autoResponse = await fetch(autoUpdateUrl, autoFetchConfig);
            if (autoResponse.ok) {
                console.log('Automobile update SUCCESSFUL!', await autoResponse.json());
            } else {
                throw new Error('Automobile update FAILED...');
            }
            
            setFormData({
                automobile: '',
                salesperson: '',
                customer: '',
                price: '',
            })
        } else {
            throw new Error('failed to create SALE RECORD')
        }
    }
    

    const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name
        
        setFormData({
            ...formData,
            [inputName]: value
        })
    }
    
    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Record a Sale</h1>
                        <form onSubmit={handleSubmit} id="create-sale-form">
                            <div className="mb-3 mt-4">
                                <label htmlFor="automobile">Vehicle</label>
                                <select onChange={handleFormChange} value={formData.automobile} required name="automobile" id="automobile" className="form-select">
                                    <option value="">Select</option>
                                    {automobiles.map(automobile => {
                                        return (
                                            <option key={automobile.vin} value={automobile.vin}>{automobile.vin} - {automobile.year} {automobile.model.manufacturer.name} {automobile.model.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="mb-3 mt-4">
                                <label htmlFor="salesperson">Salesperson</label>
                                <select onChange={handleFormChange} value={formData.salesperson} required name="salesperson" id="salesperson" className="form-select">
                                    <option value="">Select</option>
                                    {salespeople.map(salesperson => {
                                        return (
                                            <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="mb-3 mt-4">
                                <label htmlFor="customer">Customer</label>
                                <select onChange={handleFormChange} value={formData.customer} required name="customer" id="customer" className="form-select">
                                    <option value="">Select</option>
                                    {customers.map(customer => {
                                        return (
                                            <option key={customer.id} value={customer.id}>{customer.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="mb-3 mt-4">
                                <label htmlFor="price">Price</label>
                                <input 
                                type="decimal" 
                                className="form-control" 
                                id="price"
                                name="price"
                                placeholder="Price"
                                value={formData.price}
                                onChange={handleFormChange}
                                required
                                />
                            </div>
                            <button className="btn btn-primary"> Add Sale</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )

}

export default SalesForm