import { Routes, Route } from 'react-router-dom'
import MainPage from './MainPage'
import Nav from './Nav'
import SalespersonForm from './SalespersonForm'
import Salespeople from './SalespeopleList'
import Customers from './CustomerList'
import Sales from './SalesList'
import SalesForm from './SalesForm'
import CustomerForm from './CustomerForm'
import Manufacturers from './ManufacturersList'
import ManufacturerForm from './ManufacturerForm'
import VehicleModels from './VehicleModelsList'
import VehicleModelForm from './VehicleModelForm'
import AutomobilesList from './AutomobilesList'
import AutomobileForm from './AutomobileForm'
import SalesBySalesPerson from './SalesBySalesperson'
import AppointmentList from './service/AppointmentList';
import AppointmentForm from './service/AppointmentForm';
import ServiceHistory from './service/ServiceHistory';
import TechnicianList from './service/TechnicianList';
import TechnicianForm from './service/TechnicianForm';


function App(props) {
  return (
    <>
      <Nav/>
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/salespersons/new" element={<SalespersonForm />} />
          <Route path="/salespersons" element={<Salespeople salespeople={props.salespeople}/>} />
          <Route path="/customers" element={<Customers customers={props.customers}/>} />
          <Route path="/sales" element={<Sales sales={props.sales}/>} />
          <Route path="/sales/new" element={<SalesForm />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          <Route path="/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="/manufacturers" element={<Manufacturers manufacturers={props.manufacturers}/>} />
          <Route path="/vehicle-models" element={<VehicleModels models={props.models}/>} />
          <Route path="/vehicle-models/new" element={<VehicleModelForm />} />
          <Route path="/automobiles" element={<AutomobilesList automobiles={props.automobiles}/>} />
          <Route path="/automobiles/new" element={<AutomobileForm />} />
          <Route path="/salespersons/sales" element={<SalesBySalesPerson sales={props.sales}/>} />
          <Route path="/appointments" element={< AppointmentList />} />
          <Route path="/appointments/new" element={< AppointmentForm />} />
          <Route path="/service-history" element={< ServiceHistory />} />
          <Route path="/technicians" element={< TechnicianList />} />
          <Route path="/technicians/new" element={< TechnicianForm />} />
        </Routes>
      </div>
    </>
  );
}

export default App;
