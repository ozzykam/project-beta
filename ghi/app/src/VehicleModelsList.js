import React, { useState, useEffect } from 'react'

function VehicleModels(props) {
    const [models, setVehicleModels] = useState(props.models || [])

    useEffect(() => {
        setVehicleModels(props.models)
    }, [props.models])

    const modelImage = (url, modelName) => {
        return (
            <img
                src={url}
                alt={`Model: ${modelName}`}
                style={{ maxWidth: '150px', height: 'auto'}}

            />
        )
    }

    return(
        <>
        <h1 className="display-6 fw-bold">Vehicle Models</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Model Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {models && models.map(model => (
                    <tr key={model.href}>
                        <td>{model.name}</td>
                        <td>{model.manufacturer.name}</td>
                        <td>{modelImage(model.picture_url, model.name)}</td>
                    </tr>
                ))}
            </tbody>
        </table>
        </>
    )

}

export default VehicleModels