import React, { useState, useEffect } from 'react'

function Salespeople(props) {
    const [salespeople, setSalespeople] = useState(props.salespeople || [])

    useEffect (() => {
        setSalespeople(props.salespeople)
    }, [props.salespeople])

    return (
        <>
        <h1 className="display-6 fw-bold">Salespeople</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
                {salespeople && salespeople.map(person => (
                    <tr key={person.id}>
                        <td>{person.employee_id}</td>
                        <td>{person.first_name}</td>
                        <td>{person.last_name}</td>
                    </tr>
                ))}
            </tbody>
        </table>
        </>
    )
}

export default Salespeople