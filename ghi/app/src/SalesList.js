import React, { useState, useEffect } from 'react'

function Sales(props) {
    const [sales, setSales] = useState(props.sales || [])

    useEffect (() => {
        setSales(props.sales)
    }, [props.sales])

    const formattedCurrency = (value) => {
        return new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2
        }).format(value)
    }

    return (
        <>
        <h1 className="display-6 fw-bold">Customers</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson Employee ID</th>
                    <th>Salesperson Name</th>
                    <th>Customer</th>
                    <th>Vin</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales && sales.map(sale => (
                    <tr key={sale.id}>
                        <td>{sale.salesperson_id}</td>
                        <td>{sale.salesperson_name}</td>
                        <td>{sale.customer}</td>
                        <td>{sale.automobile}</td>
                        <td>{formattedCurrency(sale.price)}</td>
                    </tr>
                ))}
            </tbody>
        </table>
        </>
    )
}

export default Sales