from django.urls import path
from .api_views import api_appointment_details, api_appointment_list, api_canceled_appointment, api_finished_appointment, api_technician_list, api_technician_detail, sold_vins


urlpatterns = [
    path("technicians/", api_technician_list, name="api_technician_list"),
    path("technicians/<int:id>/", api_technician_detail, name="api_technician_detail"),
    path("appointments/", api_appointment_list, name="api_appointment_list"),
    path("appointments/<int:id>/", api_appointment_details, name="api_appointment_detail"),
    path("appointments/<int:id>/cancel/", api_canceled_appointment, name="api_canceled_appointment"),
    path("appointments/<int:id>/finish/", api_finished_appointment, name="api_finished_appointment"),
    path("appointments/sold/", sold_vins),
]
