from django.db import models
from django.urls import reverse

class Technician(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=50, unique=True)
    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=25, unique=True)
    sold = models.BooleanField(default=False)
    def __str__(self):
        return self.vin


class Appointment(models.Model):
    id = models.AutoField(primary_key=True)
    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.CharField(max_length=100)
    vin = models.CharField(max_length=50)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        on_delete=models.CASCADE,
        related_name= "appointment"
    )
    def __str__(self):
        return f"{self.customer}/{self.status}"
