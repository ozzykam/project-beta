import json
from django.http import JsonResponse
from django.shortcuts import render
from common.json import ModelEncoder
from .models import Appointment, AutomobileVO, Technician
from django.views.decorators.http import require_http_methods

class AutomobilVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin"]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
        ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        ]
    encoders = {
        "technician": TechnicianEncoder(),
    }


def sold_vins(request):
    sold_cars = AutomobileVO.objects.filter(sold=True)
    return JsonResponse(
        sold_cars,
        encoder=AutomobilVOEncoder,
        safe=False
    )


@require_http_methods(["GET", "POST"])
def api_technician_list(request):
    if request.method == 'GET':
        technician = Technician.objects.all()
        return JsonResponse (
            {"technician":technician},
            encoder= TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_technician_detail(request, id):
    if request.method == "GET":
        technician= Technician.objects.filter(id=id)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )
    else:
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"delete": count >0})


@require_http_methods(["GET", "POST"])
def api_appointment_list(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        return JsonResponse (
            {"appointment": appointment},
            encoder= AppointmentEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            employee_id = content["technician"]["employee_id"]
            technician = Technician.objects.get(employee_id=employee_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Appointment Info"},
                status=400
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_appointment_details(request, id):
    try:
        appointment = Appointment.objects.get(id=id)

    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "Appointment not found"},
            status=400
        )
    if request.method == "GET":
        return JsonResponse(
            appointment, encoder=AppointmentEncoder, safe=False
        )
    else:
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"delete": count > 0})


@require_http_methods(["PUT"])
def api_canceled_appointment(request,id):
    try:
        appointment = Appointment.objects.get(id=id)
        if request.method =="PUT":
            appointment.status = "canceled"
            appointment.save()
            return JsonResponse({"message": "Appointment canceled successfully"})
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Appointment not found"}, status=404)


@require_http_methods(["PUT"])
def api_finished_appointment(request, id):
    try:
        appointment= Appointment.objects.get(id=id)
        if request.method =="PUT":
            appointment.status = "finished"
            appointment.save()
            return JsonResponse({"message": "Appointment finished successfully"})
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Appointment not found"}, status=404)
