import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something
from sales_rest.models import AutomobileVO

def poll():
    while True:
        print('Sales poller polling for data')
        try:
            # Write your polling logic, here
            # Do not copy entire file
            response = requests.get("http://inventory-api:8000/api/automobiles/")
            content = response.json()

            for auto in content["autos"]:
                AutomobileVO.objects.update_or_create(
                    import_automobile_href = auto["href"],
                    defaults={
                        "sold": auto["sold"],
                        "vin": auto["vin"],
                    },
                )


        except Exception as e:
            print(e, file=sys.stderr)

        time.sleep(60)


if __name__ == "__main__":
    poll()
