from django.db import models
from django.conf import settings




class AutomobileVO(models.Model):
    import_automobile_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=30)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin
    

class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=20)
    employee_id = models.CharField(max_length=100)
    name = models.CharField(max_length=200)

    def save(self, *args, **kwargs):
        self.name = f"{self.last_name}, {self.first_name}"
        super().save(*args, **kwargs)
    
    def __str__(self):
        return self.employee_id
    

class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=20, null=True)
    address = models.CharField(max_length=200)
    name = models.CharField(max_length=200)

    def save(self, *args, **kwargs):
        self.name = f"{self.last_name.upper()}, {str(self.first_name)[0].upper()}." 
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name
    

class Sale(models.Model):
    price = models.DecimalField(max_digits=12, decimal_places=2)
    automobile = models.ForeignKey(
        AutomobileVO,
        on_delete=models.CASCADE,
        related_name='automobiles',
        null=True,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        on_delete=models.CASCADE,
        related_name='sales',
        null=True,
    )
    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        related_name='purchases',
        null=True,
    )

    def __str__(self):
        return self.automobile.vin
    
    
    