from django.contrib import admin

from .models import (
    AutomobileVO,
    Salesperson,
    Sale,
    Customer,
)
# Register your models here.

@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    automobile = AutomobileVO.vin
    salesperson = Salesperson.employee_id
    customer = Customer.name
    list_display = [
        "id",
        "automobile",
        "salesperson",
        "customer",
        "price"
    ]
admin.site.register(Salesperson)
admin.site.register(Customer)

