import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import (
    Sale,
    Salesperson,
    Customer,
    AutomobileVO,
)

from .encoders import (
    SalespersonCreateEncoder,
    SalespersonDetailsEncoder,
    CustomerCreateEncoder,
    CustomerDetailsEncoder,
    SaleEncoder
)


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonDetailsEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)

        return JsonResponse(
            salesperson,
            encoder=SalespersonCreateEncoder,
            safe=False,
        )


@require_http_methods(["GET","DELETE","PUT"])
def api_show_salesperson(request,id):
    if request.method == "GET":
        salesperson = Salesperson.objects.filter(id=id)
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonDetailsEncoder,
        )
    elif request.method == "DELETE":
        count, _ = Salesperson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Salesperson.objects.filter(id=id).update(**content)
        salesperson = Salesperson.objects.get(id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonCreateEncoder,
            safe=False,

        )
    

require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customer": customers},
            encoder=CustomerDetailsEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)

        return JsonResponse(
            customer,
            encoder=CustomerCreateEncoder,
            safe=False,
        )


@require_http_methods(["GET","DELETE","PUT"])
def api_show_customer(request,id):
    if request.method == "GET":
        customer = Customer.objects.filter(id=id)
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerDetailsEncoder,
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=id).update(**content)
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerCreateEncoder,
            safe=False,

        )


@require_http_methods(["GET","POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales":sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        
        try:
            vin_num = content.get("automobile")
            vin = AutomobileVO.objects.get(vin=vin_num)
            content["automobile"] = vin
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid VIN NUMBER"},
                status=400
            )
        
        try:
            employee_id = content.get("salesperson")
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Employee Info"},
                status = 400
            )
        
        try:
            customer_id = content.get("customer")
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Customer Name"},
                status = 400
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )
    

@require_http_methods(["GET","DELETE","PUT"])
def api_show_sale(request, id):
    if request.method == "GET":
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            {"sale":sale},
            encoder=SaleEncoder,
        )
    elif request.method == "DELETE":
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        
        Sale.objects.filter(id=id).update(**content)
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )