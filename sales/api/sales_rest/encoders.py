from common.json import ModelEncoder
from decimal import Decimal

from .models import(
    AutomobileVO,
    Customer,
    Sale,
    Salesperson
)


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold"
    ]


class SalespersonCreateEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "last_name",
        "first_name",
    ]


class SalespersonDetailsEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
        "name",
    ]


class CustomerDetailsEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "phone_number",
        "address",
    ]


class CustomerCreateEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "phone_number",
        "address",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
    ]
    def get_extra_data(self, o):
        data = {}
        if o.automobile is not None:
            data["automobile"] = o.automobile.vin
            data["sold"] = o.automobile.sold
        if o.salesperson is not None:
            data["salesperson_id"] = o.salesperson.employee_id
            data["salesperson_name"] = o.salesperson.name
        if o.customer is not None:
            data["customer"] = o.customer.name
        return data

    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return super().default(obj)
